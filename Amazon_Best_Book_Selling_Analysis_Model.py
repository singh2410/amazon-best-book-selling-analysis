#!/usr/bin/env python
# coding: utf-8

# # Amazon Best Book Selling Analysis Model
# #By- Aarush Kumar
# #Dated: July 08,2021

# In[5]:


from IPython.display import Image
Image(url='https://images-na.ssl-images-amazon.com/images/G/01/AmazonBooksMarketing/Landing-Page-Store-Image/JSE3.jpg')


# In[6]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('darkgrid')


# In[7]:


df = pd.read_csv(r"/home/aarush100616/Downloads/Projects/Amazon Best Selling Book Analysis/bestsellers with categories.csv")


# In[8]:


df


# In[9]:


df.head()


# In[10]:


df.info()


# In[11]:


df.isnull().sum()


# In[12]:


df.describe()


# In[13]:


df.shape


# In[14]:


df.size


# In[15]:


# 'Name' Top 20 books that have highest sold
Top_sold = df['Name'].value_counts().iloc[:20]


# In[51]:


plt.figure(figsize=(8,15))
sns.barplot(x = Top_sold.values , y = Top_sold.index , color='#cc0000', alpha=.7)
plt.title('Top 30 books that have highest sold', fontsize=20)
plt.xlabel('Count' , fontsize=15)
plt.ylabel('Name of The Book' , fontsize=15)
plt.xticks(fontsize=15 )
plt.yticks(fontsize=15)
plt.show()


# In[17]:


Top_Authors = [df[df['Name']==x]['Author'].iloc[0] for x in  Top_sold.index]


# In[18]:


# Top_Authors
Top_sold.values


# In[52]:


plt.figure(figsize=(22,6))
plot = sns.barplot(x = Top_Authors, y =  Top_sold.values , color='#cc0000', alpha=.7)
for bar in plot.patches:
    plot.annotate(
        format(bar.get_height(), '.2f'),
        (bar.get_x() + bar.get_width()/2,
        bar.get_height()),
        ha='center', va='center', size=15 , xytext=(0,8),
        textcoords='offset points'
    )
plt.title('Top 20 Authors that have the highest number of sold  books', fontsize=20)
plt.xlabel('Count' , fontsize=15)
plt.ylabel('Author' , fontsize=15)
plt.xticks(fontsize=15 , rotation=90)
plt.yticks(fontsize=15)
plt.show()


# In[20]:


# Top 20 Authors that have the highest number of published  books
Author = df['Author'].value_counts().iloc[:20]


# In[53]:


plt.figure(figsize=(22,6))
plot = sns.barplot(x = Author.index , y = Author.values , color='#cc0000', alpha=.7)
for bar in plot.patches:
    plot.annotate(
        format(bar.get_height(), '.2f'),
        (bar.get_x() + bar.get_width()/2,
        bar.get_height()),
        ha='center', va='center', size=15 , xytext=(0,8),
        textcoords='offset points'
    )
plt.title('Top 20 Authors that have the highest number of published  books', fontsize=20)
plt.xlabel('Count' , fontsize=15)
plt.ylabel('Author' , fontsize=15)
plt.xticks(fontsize=15 , rotation=90)
plt.yticks(fontsize=15)
plt.show()


# In[22]:


Rating = df['User Rating'].value_counts()


# In[54]:


plt.figure(figsize=(22,6))
plot = sns.barplot(x = Rating.index , y = Rating.values , color='#cc0000', alpha=.7 , order=Rating.index)
for bar in plot.patches:
    plot.annotate(
        format(bar.get_height(), '.2f'),
        (bar.get_x() + bar.get_width()/2,
        bar.get_height()),
        ha='center', va='center', size=15 , xytext=(0,8),
        textcoords='offset points'
    )
plt.title('User Rating', fontsize=20)
plt.xlabel('Rating' , fontsize=15)
plt.ylabel('count' , fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.show()


# In[24]:


# Best Books that have highest Reviewer
Best_B_R = df.sort_values('Reviews', axis=0, ascending=False)[['Name','Reviews']].iloc[:40]
Best_B_R = Best_B_R[~Best_B_R.duplicated()]


# In[55]:


plt.figure(figsize=(15,22))
plot = sns.barplot(y = Best_B_R['Name'].values , x = Best_B_R['Reviews'].values , color='#cc0000', alpha=.7 , order=Best_B_R['Name'].values)
plt.title('Best Books that have highest Reviewer', fontsize=30)
plt.xlabel('Reviewer' , fontsize=20)
plt.ylabel('Books' , fontsize=20)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.show()


# In[26]:


# Best Books that have highest Rating
Best_B_Rating = df.sort_values('User Rating', axis=0, ascending=False)[['Name','User Rating']].iloc[:40]
Best_B_Rating = Best_B_Rating[~Best_B_Rating.duplicated()]


# In[56]:


plt.figure(figsize=(15,22))
plot = sns.barplot(y = Best_B_Rating['Name'].values , x = Best_B_Rating['User Rating'].values , color='#cc0000', alpha=.7 , order=Best_B_Rating['Name'].values)
plt.title('Best Books that have highest Rating', fontsize=30)
plt.xlabel('Rating' , fontsize=20)
plt.ylabel('Books' , fontsize=20)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.show()


# In[28]:


# Best Authors that have highest Rating
Best_A_Rating = df.sort_values('User Rating', axis=0, ascending=False)[['Author','User Rating']].iloc[:40]
Best_A_Rating = Best_A_Rating[~Best_A_Rating.duplicated()]


# In[57]:


plt.figure(figsize=(22,6))
plot = sns.barplot(x=Best_A_Rating['Author'].values, y=Best_A_Rating['User Rating'].values, color='#cc0000', alpha=.7,order= Best_A_Rating['Author'].values)
for bar in plot.patches:
    plot.annotate(
        format(bar.get_height(), '.2f'),
        (bar.get_x() + bar.get_width()/2,
        bar.get_height()),
        ha='center', va='center', size=15 , xytext=(0,8),
        textcoords='offset points'
    )
plt.title('Best Authors that have highest Rating', fontsize=20)
plt.xlabel('Authors' , fontsize=15)
plt.ylabel('count' , fontsize=15)
plt.xticks(fontsize=15, rotation=45)
plt.yticks(fontsize=15 )
plt.show()


# In[30]:


plt.figure(figsize=(7,5))
sns.boxplot(y=df['Price'])
plt.title('Price')
plt.show()


# In[58]:


Year = df['Year'].value_counts()
plt.figure(figsize=(11,6))
plot = sns.barplot(x=Year.index, y=Year.values, color='#cc0000', alpha=.7)
for bar in plot.patches:
    plot.annotate(
        format(bar.get_height(), '.2f'),
        (bar.get_x() + bar.get_width()/2,
        bar.get_height()),
        ha='center', va='center', size=15 , xytext=(0,8),
        textcoords='offset points'
    )
plt.title('count of Books that published in each year', fontsize=20)
plt.xlabel('Years' , fontsize=15)
plt.ylabel('count' , fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15 )
plt.show()


# In[60]:


Genre = df['Genre'].value_counts()
plt.figure(figsize=(11,6))
plt.pie(Genre, colors=['#cc0000','#990000'],autopct='%1.f%%',  startangle=90, shadow=True ,labels=Genre.index)
plt.title('Genre of Books', fontsize=20 )
plt.show()


# In[62]:


sns.relplot(x = 'User Rating', y = 'Reviews', data = df, kind = 'line', hue = 'Genre',
            palette = {'Fiction': '#cc0000', 'Non Fiction': '#990000'}, linewidth = 3)
plt.title('Relation between User Rating and Reviews with Genre as a hue')
plt.tight_layout()


# In[63]:


sns.relplot(x = 'User Rating', y = 'Price', data = df, kind = 'line', 
            color ='#cc0000' , linewidth = 3)
plt.title('Relation between User Rating and Reviews')
plt.tight_layout()


# ## The Best Book On each year from user rating and Reviews.

# In[36]:


y = np.arange(2009,2020)
name ,rating ,review,typ,authore,price,year= [],[],[],[],[],[],[]
for i in y:
    
    df_y = df[df['Year']==i]
    d = df_y[df_y['User Rating'] == df_y['User Rating'].max()].sort_values('Reviews',ascending=False).iloc[0]
    name.append(d['Name'])
    rating.append(d['User Rating'])
    review.append(d['Reviews'])
    typ.append(d['Genre'])
    authore.append(d['Author'])
    price.append(d['Price'])
    year.append(d['Year'])
    
pd.DataFrame(data=[name,authore ,rating ,review ,typ,price,year] , index=['name' ,'authore','rating' ,'review' ,'typ','price','year']).reset_index()


# ## The Worst book On each year from user rating and Reviews.

# In[37]:


y = np.arange(2009,2020)
name ,rating ,review,typ,authore,price,year= [],[],[],[],[],[],[]
for i in y:
    df_y = df[df['Year']==i]
    d = df_y[df_y['User Rating'] == df_y['User Rating'].min()].sort_values('Reviews',ascending=True).iloc[0]
    name.append(d['Name'])
    rating.append(d['User Rating'])
    review.append(d['Reviews'])
    typ.append(d['Genre'])
    authore.append(d['Author'])
    price.append(d['Price'])
    year.append(d['Year'])
pd.DataFrame(data=[name,authore ,rating ,review ,typ,price,year] , index=['name' ,'authore','rating' ,'review' ,'typ','price','year']).reset_index()


# In[64]:


sns.histplot(df['User Rating'],bins=15, color='#cc0000' )
plt.title("User Rating distribution", fontsize=20)
plt.show()


# In[65]:


sns.histplot(df['Reviews'],bins=15, color='#cc0000' )
plt.title("Reviews Distribution", fontsize=20)
plt.show()


# In[66]:


d = df['Reviews']
d = d.mask((d - d.mean()).abs() > 2 * d.std())
sns.histplot(d , color='#cc0000')
print(d.mean())


# In[67]:


sns.histplot(df['Price']  , color='#cc0000')
plt.title('Price Distribution', fontsize=20)
print('Mean is = ',d.mean())
plt.show()


# In[68]:


d = df['Price']
d = d.mask((d - d.mean()).abs() > 2 * d.std())
sns.histplot(d , color='#cc0000')
print('Mean is = ',d.mean())
plt.show()


# In[69]:


sns.histplot(x=df['User Rating'], y=df['Price'],hue=df['Genre'], palette=['#cc0000','#990000'],)
plt.title('Distribution of Price with User Rating and hue by Genre',  fontsize=20)
plt.show()


# In[44]:


mean_year = df.groupby('Year').mean()
median_year = df.groupby('Year').median()


# In[45]:


rating = pd.DataFrame( mean_year['User Rating'].values , median_year.index , columns=['mean'])
rating['median'] = median_year['User Rating'].values


# In[70]:


sns.relplot(data=rating ,kind='line', palette=['#cc0000','#990000'] , linewidth = 3)
plt.title("Mean vs Median of Rating in each year",fontsize=20)
plt.ylabel('User Rating',fontsize=15)
plt.xlabel('Year',fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.legend(fontsize=15,loc =(1.01,.85))
plt.show()


# In[47]:


price = pd.DataFrame( mean_year['Price'].values , mean_year.index , columns=['mean'])
price['median'] = median_year['Price'].values


# In[71]:


sns.relplot(data=price ,kind='line', palette=['#cc0000','#990000'] , linewidth = 3)
plt.title("Mean vs Median of Price in each year",fontsize=20)
plt.ylabel('Price',fontsize=15)
plt.xlabel('Year',fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.legend(fontsize=15, loc=(1.01,.85))
plt.show()


# In[72]:


sns.scatterplot(x=df['User Rating'], y=df['Reviews'] ,color='#cc0000')


# In[73]:


sns.scatterplot(x=df['User Rating'], y=df['Price'],color='#cc0000')

